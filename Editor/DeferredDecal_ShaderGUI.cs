﻿/*

*DeferredDecal_ShaderGUI.cs* is not available under any license.
Copyright (c) 2018 Alex Short

*/

/*

*DeferredDecal_ShaderGUI.cs* is derived from *StandardShaderGUI.cs*.
*StandardShaderGUI.cs* is Copyright (c) 2016 Unity Technologies.
*StandardShaderGUI.cs* is licensed under the MIT license (See *MIT License* in *LICENSES.md*).

You can find all versions of *StandardShaderGUI.cs* here: https://unity3d.com/get-unity/download/archive
You can download the version of *StandardShaderGUI.cs* that *DeferredDecal_ShaderGUI.cs* was derived from here: https://download.unity3d.com/download_unity/21ae32b5a9cb/builtin_shaders-2017.4.3f1.zip

*/

using UnityEngine;
using UnityEditor;
using UnityEngine.Rendering;
using System.Linq;

namespace Vostn.Decala.Editor
{
	public class DeferredDecal_ShaderGUI : ShaderGUI
	{
		private enum Workflow
		{
			Specular,
			Metallic,
		}

		private enum MaskMode
		{
			AllLayers = 16,
			OnlyFirstTwoLayers = 0,
		}

		private enum KeywordState
		{
			On,
			Off,
			Mixed,
		}

		private enum TransparencyMode
		{
			Cutout,
			Fade,
			//Transparent,
		}

		private static class Styles
		{
			public static readonly GUIContent fadeAngleText = new GUIContent("Fade Angle", "Geometry direction threshold for material fade");
			public static readonly GUIContent fadeDirectionText = new GUIContent("Fade Direction", "The direction used for material fade: 0 = projector up, 1 = direction to pivot");
			public static readonly GUIContent fadeRangeText = new GUIContent("Fade Range", "Amount of geometry direction change over which fade occurs");

			public static readonly GUIContent albedoMapText = new GUIContent("Albedo and Opacity", "Albedo (RGB) and Opacity (A), and Albedo Color");
			public static readonly GUIContent opacityCutoffText = new GUIContent("Opacity Cutoff", "Minimum opacity allowed for pixel");

			public static readonly GUIContent specularSmoothMapText = new GUIContent("Specular and Smoothness", "Specular (RGB) and Smoothness (A), and Specular Color");

			public static readonly GUIContent mixedSpecMetalSmoothMapText = new GUIContent("Metallic/Specular and Smoothness", "Metallic/Specular (R/RGB) and Smoothness (A), and Specular Color");

			public static readonly GUIContent metallicSmoothMapText = new GUIContent("Metallic and Smoothness", "Metallic (R) and Smoothness (A)");
			public static readonly GUIContent metallicText = new GUIContent("Metallic Scale", "Metallic Scale");

			public static readonly GUIContent smoothnessText = new GUIContent("Smoothness Scale", "Smoothness Scale");

			public static readonly GUIContent normalMapText = new GUIContent("Normal Map", "Normal Map (RGB), and Normal Scale");

			public static readonly GUIContent emissionMapText = new GUIContent("Emission", "Emission (RGB) and Emission Scale (A), and Emission Color");

			public static readonly string decalOptionsText = "Decal Options";
			public static readonly string decalStyleText = "Decal Style";
			public static readonly string fadeSettingsText = "Direction Fade Settings";
			public static readonly string materialSettingsText = "Material Settings";
			public static readonly string advancedSettingsText = "Advanced Settings";
		}

		private static class ToggleKeywords
		{
			public static readonly string alphaTestKeyword = "_ALPHATEST_ON";
			public static readonly string alphaFadeKeyword = "_ALPHABLEND_ON";
			public static readonly string transparencyKeyword = "_ALPHAPREMULTIPLY_ON";

			public static readonly string metallicKeyword = "_METALLIC";

			public static readonly string albedoSpecKeyword = "_ALBEDOSPEC";
			public static readonly string smoothKeyword = "_SMOOTH";
			public static readonly string smoothBlendKeyword = "_SMOOTHBLEND";
			public static readonly string normalKeyword = "_NORMAL";
			public static readonly string normal1Keyword = "_NORMAL1";
			public static readonly string normal2Keyword = "_NORMAL2";
			public static readonly string emissionKeyword = "_EMISSION";
			public static readonly string emission1Keyword = "_EMISSION1";
			public static readonly string emission2Keyword = "_EMISSION2";
		}

		private static class ToggleText
		{
			public static readonly string metalToggleText = "Metallic";
			public static readonly string specToggleText = "Specular";

			public static readonly string albedoMetalText = "Modify Albedo and Metallicity";
			public static readonly string albedoSpecText = "Modify Albedo and Specularity";
			public static readonly string albedoMixedText = "Modify Albedo and Metallicity/Specular";

			public static readonly string smoothText = "Modify Smoothness";
			public static readonly string smoothBlendText = "Use Alpha Blended Smoothness (Disables Instancing)";

			public static readonly string normalText = "Modify Normals";
			public static readonly string emissionText = "Modify Emission";
		}

		private MaterialProperty fadeAngle = null;
		private MaterialProperty fadeDirection = null;
		private MaterialProperty fadeRange = null;

		private MaterialProperty albedoMap = null;
		private MaterialProperty albedoColor = null;
		private MaterialProperty opacityCutoff = null;

		private MaterialProperty specularSmoothnessMap = null;
		private MaterialProperty specularColor = null;

		private MaterialProperty metallicScale = null;

		private MaterialProperty smoothnessScale = null;

		private MaterialProperty normalMap = null;
		private MaterialProperty normalScale = null;

		private MaterialProperty emissionMap = null;
		private MaterialProperty emissionColor = null;

		private MaterialProperty maskMode = null;

		private MaterialProperty transparencyMode = null;
		private MaterialProperty srcBlend = null;
		private MaterialProperty dstBlend = null;

		private MaterialProperty smoothBlendType = null;

		private MaterialProperty specSrcBlend = null;
		private MaterialProperty specDstBlend = null;

		private MaterialEditor materialEditor;

		public void FindProperties(MaterialProperty[] props)
		{
			fadeAngle = FindProperty("_FadeAngle", props);
			fadeDirection = FindProperty("_FadeDirection", props);
			fadeRange = FindProperty("_FadeRange", props);

			albedoMap = FindProperty("_MainTex", props);
			albedoColor = FindProperty("_Color", props);
			opacityCutoff = FindProperty("_Cutoff", props);

			specularSmoothnessMap = FindProperty("_SpecularSmoothnessMap", props, false);
			specularColor = FindProperty("_SpecularColor", props, false);

			metallicScale = FindProperty("_MetallicScale", props, false);

			smoothnessScale = FindProperty("_SmoothnessScale", props);

			normalMap = FindProperty("_NormalMap", props);
			normalScale = FindProperty("_NormalScale", props);

			emissionMap = FindProperty("_EmissionMap", props);
			emissionColor = FindProperty("_EmissionColor", props);

			maskMode = FindProperty("_StencilRef", props);

			transparencyMode = FindProperty("_TransparencyMode", props);
			srcBlend = FindProperty("_SrcBlend", props);
			dstBlend = FindProperty("_DstBlend", props);

			smoothBlendType = FindProperty("_SmoothBlendType", props);

			specSrcBlend = FindProperty("_SpecSrcBlend", props);
			specDstBlend = FindProperty("_SpecDstBlend", props);
		}

		public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
		{
			FindProperties(properties);
			this.materialEditor = materialEditor;
			Material[] materials = materialEditor.targets.Cast<Material>().ToArray();

			ShaderPropertiesGUI(materials);
		}

		public void ShaderPropertiesGUI(Material[] materials)
		{
			EditorGUIUtility.labelWidth = 0f;

			GUILayout.Label(Styles.decalOptionsText, EditorStyles.boldLabel);
			DoToggleArea(materials);

			EditorGUILayout.Space();

			GUILayout.Label(Styles.decalStyleText, EditorStyles.boldLabel);
			DoStyleArea(materials);

			EditorGUILayout.Space();

			GUILayout.Label(Styles.fadeSettingsText, EditorStyles.boldLabel);
			DoFadeArea();

			EditorGUILayout.Space();

			GUILayout.Label(Styles.materialSettingsText, EditorStyles.boldLabel);
			DoAlbedoArea(materials);
			DoSpecularMetallicArea(materials);
			DoSmoothnessArea(materials);
			DoNormalArea(materials);
			DoEmissionArea(materials);

			EditorGUILayout.Space();

			GUILayout.Label(Styles.advancedSettingsText, EditorStyles.boldLabel);
			materialEditor.RenderQueueField();
			materialEditor.EnableInstancingField();
			materialEditor.DoubleSidedGIField();

			UpdateMaterialSettings(materials);
		}

		private bool DrawOptionToggle(Material[] materials, string keyword, string text, bool invertState = false)
		{
			EditorGUI.BeginChangeCheck();
			KeywordState originalState = GetKeyword(materials, keyword);
			bool stateWasMixed = EditorGUI.showMixedValue;
			if (originalState == KeywordState.Mixed)
				EditorGUI.showMixedValue = true;
			KeywordState targetKeywordState = KeywordState.Off;
			if (invertState)
				targetKeywordState = KeywordState.On;
			bool newValue = EditorGUILayout.ToggleLeft(text, originalState != targetKeywordState);
			if (EditorGUI.EndChangeCheck())
			{
				if(invertState)
					SetKeyword(materials, keyword, !newValue);
				else
					SetKeyword(materials, keyword, newValue);
			}
			if (originalState == KeywordState.Mixed)
				EditorGUI.showMixedValue = stateWasMixed;
			return newValue;
		}

		private void DoToggleArea(Material[] materials)
		{
			EditorGUILayout.BeginVertical(new GUIStyle("Box"));
			EditorGUILayout.LabelField("Material Mode", EditorStyles.boldLabel);

			DrawOptionToggle(materials, ToggleKeywords.metallicKeyword, ToggleText.metalToggleText);
			DrawOptionToggle(materials, ToggleKeywords.metallicKeyword, ToggleText.specToggleText, true);

			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical(new GUIStyle("Box"));
			EditorGUILayout.LabelField("Enables First Pass", EditorStyles.boldLabel);

			KeywordState metallicState = GetKeyword(materials, ToggleKeywords.metallicKeyword);
			if (metallicState == KeywordState.Mixed)
				DrawOptionToggle(materials, ToggleKeywords.albedoSpecKeyword, ToggleText.albedoMixedText);
			else if (metallicState == KeywordState.On)
				DrawOptionToggle(materials, ToggleKeywords.albedoSpecKeyword, ToggleText.albedoMetalText);
			else if (metallicState == KeywordState.Off)
				DrawOptionToggle(materials, ToggleKeywords.albedoSpecKeyword, ToggleText.albedoSpecText);
			
			EditorGUILayout.EndVertical();
			EditorGUILayout.BeginVertical(new GUIStyle("Box"));
			EditorGUILayout.LabelField("Enables Second Pass", EditorStyles.boldLabel);

			DrawOptionToggle(materials, ToggleKeywords.smoothKeyword, ToggleText.smoothText);
			
			if (GetKeyword(materials, ToggleKeywords.smoothKeyword) != KeywordState.Off && (transparencyMode.hasMixedValue || (int)transparencyMode.floatValue != (int)TransparencyMode.Cutout))
			{
				EditorGUILayout.BeginVertical(new GUIStyle("Box"));
				EditorGUILayout.LabelField("Enables Both Passes", EditorStyles.boldLabel);
				DrawOptionToggle(materials, ToggleKeywords.smoothBlendKeyword, ToggleText.smoothBlendText);
				EditorGUILayout.EndVertical();
			}

			EditorGUILayout.EndVertical();
			EditorGUILayout.BeginVertical(new GUIStyle("Box"));
			EditorGUILayout.LabelField("Enables Either Pass", EditorStyles.boldLabel);

			DrawOptionToggle(materials, ToggleKeywords.normalKeyword, ToggleText.normalText);
			DrawOptionToggle(materials, ToggleKeywords.emissionKeyword, ToggleText.emissionText);

			EditorGUILayout.EndVertical();
		}

		private void UpdateMaterialSettings(Material[] materials)
		{
			if (!transparencyMode.hasMixedValue && (int)transparencyMode.floatValue == (int)TransparencyMode.Cutout)
				SetKeyword(materials, ToggleKeywords.smoothBlendKeyword, false);

			foreach(Material mat in materials)
			{
				bool smoothBlendMode = false;
				bool pass1 = false;
				bool pass2 = false;

				if (mat.IsKeywordEnabled(ToggleKeywords.albedoSpecKeyword))
				{
					pass1 = true;
					if(mat.IsKeywordEnabled(ToggleKeywords.normalKeyword))
					{
						mat.EnableKeyword(ToggleKeywords.normal1Keyword);
						mat.DisableKeyword(ToggleKeywords.normal2Keyword);
					}
					if(mat.IsKeywordEnabled(ToggleKeywords.emissionKeyword))
					{
						mat.EnableKeyword(ToggleKeywords.emission1Keyword);
						mat.DisableKeyword(ToggleKeywords.emission2Keyword);
					}
				}
				else
				{
					if(mat.IsKeywordEnabled(ToggleKeywords.normalKeyword))
					{
						mat.EnableKeyword(ToggleKeywords.normal2Keyword);
						mat.DisableKeyword(ToggleKeywords.normal1Keyword);
					}
					if(mat.IsKeywordEnabled(ToggleKeywords.emissionKeyword))
					{
						mat.EnableKeyword(ToggleKeywords.emission2Keyword);
						mat.DisableKeyword(ToggleKeywords.emission1Keyword);
					}
				}

				if(!mat.IsKeywordEnabled(ToggleKeywords.normalKeyword))
				{
					mat.DisableKeyword(ToggleKeywords.normal1Keyword);
					mat.DisableKeyword(ToggleKeywords.normal2Keyword);
				}
				if(!mat.IsKeywordEnabled(ToggleKeywords.emissionKeyword))
				{
					mat.DisableKeyword(ToggleKeywords.emission1Keyword);
					mat.DisableKeyword(ToggleKeywords.emission2Keyword);
				}


				if (mat.IsKeywordEnabled(ToggleKeywords.smoothKeyword) || mat.IsKeywordEnabled(ToggleKeywords.normal2Keyword) ||  mat.IsKeywordEnabled(ToggleKeywords.emission2Keyword))
					pass2 = true;

				if (mat.IsKeywordEnabled(ToggleKeywords.smoothBlendKeyword) && mat.IsKeywordEnabled(ToggleKeywords.smoothKeyword))
				{
					smoothBlendMode = true;
					mat.SetFloat(smoothBlendType.name, (int)BlendMode.One);
					mat.enableInstancing = false;
				}
				else
				{
					mat.SetFloat(smoothBlendType.name, (int)BlendMode.Zero);
				}
				
				if(pass1)
				{
					mat.SetFloat(specSrcBlend.name, (int)BlendMode.SrcAlpha);
					mat.SetFloat(specDstBlend.name, (int)BlendMode.OneMinusSrcAlpha);
				}
				else
				{
					mat.SetFloat(specSrcBlend.name, (int)BlendMode.Zero);
					mat.SetFloat(specDstBlend.name, (int)BlendMode.One);
				}

				mat.SetShaderPassEnabled(mat.GetPassName(0), pass1 || smoothBlendMode);
				mat.SetShaderPassEnabled(mat.GetPassName(1), pass2 || smoothBlendMode);
			}
		}

		private void DoStyleArea(Material[] materials)
		{
			using (var maskChange = new EditorGUI.ChangeCheckScope())
			{
				MaskMode newMaskMode = (MaskMode)EditorGUILayout.EnumPopup("Layer Mask Mode", (MaskMode)((int)maskMode.floatValue));
				if (maskChange.changed)
					maskMode.floatValue = (int)newMaskMode;
			}

			using (var transparencyChange = new EditorGUI.ChangeCheckScope())
			{
				TransparencyMode newTransparencyMode = (TransparencyMode)EditorGUILayout.EnumPopup("Transparency Mode", (TransparencyMode)((int)transparencyMode.floatValue));
				if (transparencyChange.changed)
				{
					transparencyMode.floatValue = (int)newTransparencyMode;
					if (newTransparencyMode == TransparencyMode.Cutout)
					{
						SetKeyword(materials, ToggleKeywords.transparencyKeyword, false);
						SetKeyword(materials, ToggleKeywords.alphaFadeKeyword, false);
						SetKeyword(materials, ToggleKeywords.alphaTestKeyword, true);
						srcBlend.floatValue = (int)BlendMode.SrcAlpha;
						dstBlend.floatValue = (int)BlendMode.OneMinusSrcAlpha;
					}
					else if (newTransparencyMode == TransparencyMode.Fade)
					{
						SetKeyword(materials, ToggleKeywords.transparencyKeyword, false);
						SetKeyword(materials, ToggleKeywords.alphaFadeKeyword, true);
						SetKeyword(materials, ToggleKeywords.alphaTestKeyword, false);
						srcBlend.floatValue = (int)BlendMode.SrcAlpha;
						dstBlend.floatValue = (int)BlendMode.OneMinusSrcAlpha;
					}
				}
			}
		}

		private void DoFadeArea()
		{
			materialEditor.ShaderProperty(fadeAngle, Styles.fadeAngleText.text);
			materialEditor.ShaderProperty(fadeDirection, Styles.fadeDirectionText.text);
			materialEditor.ShaderProperty(fadeRange, Styles.fadeRangeText.text);
		}

		private void DoAlbedoArea(Material[] materials)
		{
			materialEditor.TexturePropertySingleLine(Styles.albedoMapText, albedoMap, albedoColor);
			if (albedoMap.textureValue != null)
				materialEditor.TextureScaleOffsetProperty(albedoMap);
			if (GetKeyword(materials, ToggleKeywords.alphaTestKeyword) != KeywordState.Off || (GetKeyword(materials, ToggleKeywords.smoothBlendKeyword) != KeywordState.On && GetKeyword(materials, ToggleKeywords.smoothKeyword) != KeywordState.Off))
				materialEditor.ShaderProperty(opacityCutoff, Styles.opacityCutoffText);
		}

		private void DoSpecularMetallicArea(Material[] materials)
		{
			bool hasTexture = false;
			KeywordState metallicKeywordState = GetKeyword(materials, ToggleKeywords.metallicKeyword);
			KeywordState albedoSpecState = GetKeyword(materials, ToggleKeywords.albedoSpecKeyword);
			KeywordState smoothState = GetKeyword(materials, ToggleKeywords.smoothKeyword);

			if (albedoSpecState == KeywordState.Off && smoothState == KeywordState.Off)
				return;

			hasTexture = specularSmoothnessMap.textureValue != null;
			if (metallicKeywordState == KeywordState.Off)
			{
				if (albedoSpecState != KeywordState.Off)
					materialEditor.TexturePropertySingleLine(Styles.specularSmoothMapText, specularSmoothnessMap, specularColor);
				else
					materialEditor.TexturePropertySingleLine(Styles.specularSmoothMapText, specularSmoothnessMap);
			}
			else if (metallicKeywordState == KeywordState.On)
			{
				materialEditor.TexturePropertySingleLine(Styles.metallicSmoothMapText, specularSmoothnessMap);
				if (albedoSpecState != KeywordState.Off)
					materialEditor.ShaderProperty(metallicScale, Styles.metallicText, 2);
			}
			else
			{
				// Mixed Mode
				bool wasMixedValue = EditorGUI.showMixedValue;
				EditorGUI.showMixedValue = true;

				if (albedoSpecState != KeywordState.Off)
					materialEditor.TexturePropertySingleLine(Styles.mixedSpecMetalSmoothMapText, specularSmoothnessMap, specularColor);
				else
					materialEditor.TexturePropertySingleLine(Styles.mixedSpecMetalSmoothMapText, specularSmoothnessMap);
				if (albedoSpecState != KeywordState.Off)
					materialEditor.ShaderProperty(metallicScale, Styles.metallicText, 2);

				EditorGUI.showMixedValue = wasMixedValue;
			}

			if (hasTexture)
				materialEditor.TextureScaleOffsetProperty(specularSmoothnessMap);
		}

		private void DoSmoothnessArea(Material[] materials)
		{
			if (GetKeyword(materials, ToggleKeywords.smoothKeyword) != KeywordState.Off)
			{
				int indentation = 2;
				materialEditor.ShaderProperty(smoothnessScale, Styles.smoothnessText, indentation);
			}
		}

		private void DoNormalArea(Material[] materials)
		{
			if (GetKeyword(materials, ToggleKeywords.normalKeyword) != KeywordState.Off)
			{
				materialEditor.TexturePropertySingleLine(Styles.normalMapText, normalMap, normalMap.textureValue != null ? normalScale : null);
				if (normalScale.floatValue != 1
					&& UnityEditorInternal.InternalEditorUtility.IsMobilePlatform(EditorUserBuildSettings.activeBuildTarget)
					&& materialEditor.HelpBoxWithButton(new GUIContent("Bump scale is not supported on mobile platforms"), new GUIContent("Fix Now")))
				{
					normalScale.floatValue = 1;
				}
				if (normalMap.textureValue != null)
					materialEditor.TextureScaleOffsetProperty(normalMap);
			}
		}

		private void DoEmissionArea(Material[] materials)
		{
			if (GetKeyword(materials, ToggleKeywords.emissionKeyword) != KeywordState.Off)
			{
				bool hadEmissionTexture = emissionMap.textureValue != null;

				materialEditor.TexturePropertyWithHDRColor(Styles.emissionMapText, emissionMap, emissionColor, false);

				float brightness = emissionColor.colorValue.maxColorComponent;
				if (emissionMap.textureValue != null && !hadEmissionTexture && brightness <= 0f)
					emissionColor.colorValue = Color.white;

				//Not yet supported
				EditorGUI.BeginDisabledGroup(true);
				materialEditor.LightmapEmissionFlagsProperty(MaterialEditor.kMiniTextureFieldLabelIndentLevel, true);
				if (emissionMap.textureValue != null)
					materialEditor.TextureScaleOffsetProperty(emissionMap);
				EditorGUI.EndDisabledGroup();
			}
		}

		private static void SetKeyword(Material[] materials, string keyword, bool state)
		{
			foreach (Material mat in materials)
			{
				if (state)
					mat.EnableKeyword(keyword);
				else
					mat.DisableKeyword(keyword);
			}
		}

		private static KeywordState GetKeyword(Material[] materials, string keyword)
		{
			KeywordState state = (KeywordState)(-1);
			foreach (Material mat in materials)
			{
				if ((int)state == -1)
				{
					if (mat.IsKeywordEnabled(keyword))
						state = KeywordState.On;
					else
						state = KeywordState.Off;
				}
				else if (state == KeywordState.On)
				{
					if (!mat.IsKeywordEnabled(keyword))
						return KeywordState.Mixed;
				}
				else if (state == KeywordState.Off)
				{
					if (mat.IsKeywordEnabled(keyword))
						return KeywordState.Mixed;
				}
				else
					throw new System.NotImplementedException();
			}
			return state;
		}
	}
}