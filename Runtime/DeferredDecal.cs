﻿using UnityEngine;
using UnityEngine.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Vostn.Decala
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(MeshRenderer))]
	public class DeferredDecal : MonoBehaviour
	{
		[SerializeField]
		private Material material;

		private Material registeredSelfMaterial;
		private Material registeredRendererMaterial;
		private MeshRenderer meshRendererSurrogate;
		private Material[] materialArray;

		private Transform cachedTransform;

		private int decalIndex = -1;

		public Material DecalMaterial
		{
			get
			{
				ValidateMeshRendererReference();
				return meshRendererSurrogate.sharedMaterial;
			}
		}

		private void ValidateMaterialArray()
		{
			if (materialArray == null || materialArray.Length != 1)
				materialArray = new Material[1];
		}

		private void Awake()
		{
			cachedTransform = transform;
		}

		private void OnEnable()
		{
			decalIndex = -1;
			ValidateState();
			if(material != null)
				Renderer.AddDecal(this);
		}

		private void ValidateMeshRendererReference()
		{
			if (meshRendererSurrogate == null)
				meshRendererSurrogate = GetComponent<MeshRenderer>();
		}

		private void ValidateState()
		{
			ValidateMeshRendererReference();
			ValidateMaterialArray();

			if (enabled)
			{
				meshRendererSurrogate.enabled = false;
				meshRendererSurrogate.receiveShadows = true;
				meshRendererSurrogate.reflectionProbeUsage = ReflectionProbeUsage.BlendProbesAndSkybox;
				meshRendererSurrogate.shadowCastingMode = ShadowCastingMode.Off;
				meshRendererSurrogate.motionVectorGenerationMode = MotionVectorGenerationMode.ForceNoMotion;
				gameObject.isStatic = false;

				if (registeredRendererMaterial != meshRendererSurrogate.sharedMaterial || registeredSelfMaterial != material)
				{
					Material previousMaterial = null;
					if (registeredRendererMaterial != meshRendererSurrogate.sharedMaterial)
					{
						previousMaterial = registeredRendererMaterial;
						registeredSelfMaterial = registeredRendererMaterial = material = meshRendererSurrogate.sharedMaterial;
					}
					else if (registeredSelfMaterial != material)
					{
						previousMaterial = registeredSelfMaterial;
						registeredSelfMaterial = registeredRendererMaterial = meshRendererSurrogate.sharedMaterial = material;
					}
					materialArray[0] = material;
					meshRendererSurrogate.sharedMaterials = materialArray;
					if (decalIndex != -1)
						Renderer.UpdateDecal(this, previousMaterial);
				}
				meshRendererSurrogate.lightProbeUsage = LightProbeUsage.BlendProbes;
			}
		}

		public void OnDisable()
		{
			Renderer.RemoveDecal(this);
		}

#if UNITY_EDITOR

		private void DrawGizmo(bool selected)
		{
			Color gizmoColorCache = Gizmos.color;
			Matrix4x4 gizmosMatrixCache = Gizmos.matrix;
			Gizmos.color = new Color(0.0f, 0.7f, 1.0f, 1.0f);
			Gizmos.matrix = transform.localToWorldMatrix;

			if (selected)
			{
				ValidateState();
				Gizmos.color = new Color(0.2f, 1f, 0.5f, 1.0f);
				Gizmos.DrawWireCube(Vector3.zero, Vector3.one * 0.99f);
			}
			Gizmos.DrawWireCube(Vector3.zero, Vector3.one);

			Gizmos.color = new Color(0, 0, 0, 0);
			Gizmos.DrawCube(Vector3.zero, Vector3.one);

			Gizmos.color = gizmoColorCache;
			Gizmos.matrix = gizmosMatrixCache;
		}

		private void OnValidate()
		{
			ValidateState();
		}

		private void Reset()
		{
			OnValidate();
		}

		public void OnDrawGizmos()
		{
			DrawGizmo(false);
		}

		public void OnDrawGizmosSelected()
		{
			DrawGizmo(true);
		}

#endif

		private static class Renderer
		{
			private const int maximumDecals = 4096;
			private const int maxInstances = 1023;

			private readonly static RenderTargetIdentifier[] mrt = { BuiltinRenderTextureType.GBuffer0, BuiltinRenderTextureType.GBuffer1, BuiltinRenderTextureType.GBuffer2, BuiltinRenderTextureType.CameraTarget };
			private static Mesh cubeMesh;

			private readonly static Dictionary<Camera, CommandBuffer> commandBufferCache = new Dictionary<Camera, CommandBuffer>();
			private readonly static Dictionary<Material, HashSet<DeferredDecal>> decalsByMaterial = new Dictionary<Material, HashSet<DeferredDecal>>();
			private readonly static List<Material> decalMaterialOrder = new List<Material>();

			private readonly static CullingGroup cullingGroup = new CullingGroup();
			private readonly static BoundingSphere[] boundingSphereCache = new BoundingSphere[maximumDecals];
			private readonly static Matrix4x4[] matrixBatchCache = new Matrix4x4[maxInstances];

			private static int decalCount = 0;
			private readonly static Queue<int> decalsToUpdate = new Queue<int>();
			private readonly static DeferredDecal[] allDecals = new DeferredDecal[maximumDecals];
			private readonly static Matrix4x4[] decalMatracies = new Matrix4x4[maximumDecals];
			private readonly static bool[] decalVisibility = new bool[maximumDecals];

			private static Light surrogateLight;

			static Renderer()
			{
				Camera.onPreCull += OnPreCull;
				Camera.onPreRender += OnPreRender;
#if UNITY_EDITOR
				UnityEditor.AssemblyReloadEvents.beforeAssemblyReload += Dispose;
#endif
				cullingGroup.onStateChanged += OnCullingChanged;
			}

			private static void OnCullingChanged(CullingGroupEvent cullingGroupEvent)
			{
				decalVisibility[cullingGroupEvent.index] = cullingGroupEvent.isVisible;
			}

			public static void AddDecal(DeferredDecal decal)
			{
				if (decal.decalIndex != -1)
					return;

				if (decal.material == null)
					return;

				Material decalMaterial = decal.DecalMaterial;
				HashSet<DeferredDecal> targetSet = null;
				if (!decalsByMaterial.TryGetValue(decalMaterial, out targetSet))
				{
					targetSet = new HashSet<DeferredDecal>();
					decalsByMaterial.Add(decalMaterial, targetSet);
					decalMaterialOrder.Add(decalMaterial);
					decalMaterialOrder.Sort(CompareMaterialOrder);
				}
				targetSet.Add(decal);
				allDecals[decalCount] = decal;
				decal.decalIndex = decalCount;
				decalsToUpdate.Enqueue(decalCount);
				++decalCount;
			}

			public static void RemoveDecal(DeferredDecal decal)
			{
				RemoveDecal(decal, decal.DecalMaterial);
			}

			private static void RemoveDecal(DeferredDecal decal, Material previousMaterial)
			{
				if (decal.decalIndex == -1)
					return;
				if (decalCount == 0)
					return;
				if (previousMaterial != null)
				{
					HashSet<DeferredDecal> targetSet = null;
					if (!decalsByMaterial.TryGetValue(previousMaterial, out targetSet))
						return;
					targetSet.Remove(decal);
					if (targetSet.Count == 0)
					{
						decalsByMaterial.Remove(previousMaterial);
						decalMaterialOrder.Remove(previousMaterial);
					}
				}
				if (decalCount > 1)
				{
					DeferredDecal swapDecal = allDecals[decalCount - 1];
					swapDecal.decalIndex = decal.decalIndex;
					allDecals[decal.decalIndex] = swapDecal;
					boundingSphereCache[decal.decalIndex] = boundingSphereCache[decalCount - 1];
					decalsToUpdate.Enqueue(decal.decalIndex);
				}
				--decalCount;
				decal.decalIndex = -1;
			}

			public static void UpdateDecal(DeferredDecal decal, Material previousMaterial)
			{
				RemoveDecal(decal, previousMaterial);
				AddDecal(decal);
			}

#if UNITY_EDITOR

			private static void Dispose()
			{
				cullingGroup.Dispose();
				Camera.onPreCull -= OnPreCull;
				Camera.onPreRender -= OnPreRender;
				UnityEditor.AssemblyReloadEvents.beforeAssemblyReload -= Dispose;
				if (!Application.isPlaying)
					DestroyImmediate(surrogateLight.gameObject);
			}

#endif

			private static int CompareMaterialOrder(Material a, Material b)
			{
				return a.renderQueue - b.renderQueue;
			}

			private static void OnPreCull(Camera targetCamera)
			{
				if (targetCamera == null)
					throw new System.ArgumentNullException(nameof(targetCamera), $"Argument '{nameof(targetCamera)}' is null!");

				if (surrogateLight == null)
					surrogateLight = new GameObject("Aurora Deferred Decal Surrogate Light").AddComponent<Light>();
				surrogateLight.gameObject.hideFlags = HideFlags.DontSaveInEditor | HideFlags.NotEditable | HideFlags.HideInInspector | HideFlags.HideInHierarchy;
				surrogateLight.type = LightType.Spot;
				surrogateLight.range = 0.01f;
				surrogateLight.spotAngle = 0.01f;
				surrogateLight.shadows = LightShadows.None;
				surrogateLight.cullingMask = -4;
				surrogateLight.transform.position = targetCamera.transform.position + targetCamera.transform.forward * targetCamera.farClipPlane;

				for (int i = 0; i < decalCount; ++i)
				{
					if (allDecals[i].cachedTransform.hasChanged)
					{
						allDecals[i].cachedTransform.hasChanged = false;
						decalsToUpdate.Enqueue(i);
					}
				}

				while (decalsToUpdate.Count > 0)
				{
					int decalIndex = decalsToUpdate.Dequeue();
					if (decalIndex >= decalCount)
						continue;
					Transform transform = allDecals[decalIndex].cachedTransform;
					transform.hasChanged = false;
					decalMatracies[decalIndex] = transform.localToWorldMatrix;
					boundingSphereCache[decalIndex].position = transform.position;
					boundingSphereCache[decalIndex].radius = (transform.lossyScale * 0.5f).magnitude;
				}

				cullingGroup.SetBoundingSpheres(boundingSphereCache);
				cullingGroup.SetBoundingSphereCount(decalCount);
				cullingGroup.targetCamera = targetCamera;
			}

			private static List<float> cutoffValues = new List<float>();
			private static readonly int cutoffPropertyID = Shader.PropertyToID("_Cutoff");
			private static List<Vector4> colorValues = new List<Vector4>();
			private static readonly int colorPropertyID = Shader.PropertyToID("_Color");
			private static MaterialPropertyBlock properties = new MaterialPropertyBlock();

			private static void OnPreRender(Camera targetCamera)
			{
				if (cubeMesh == null)
					GenerateCubeMesh();

				if (targetCamera == null)
					throw new System.ArgumentNullException(nameof(targetCamera), $"Argument '{nameof(targetCamera)}' is null!");

				CommandBuffer commandBuffer;
				if (commandBufferCache.ContainsKey(targetCamera))
				{
					commandBuffer = commandBufferCache[targetCamera];
					commandBuffer.Clear();
					foreach (var buff in targetCamera.GetCommandBuffers(CameraEvent.BeforeReflections))
					{
						if (buff.name == "Aurora Deferred Decals")
							targetCamera.RemoveCommandBuffer(CameraEvent.BeforeReflections, buff);
					}
				}
				else
				{
					commandBuffer = new CommandBuffer() { name = "Aurora Deferred Decals" };
					commandBufferCache[targetCamera] = commandBuffer;
				}
				targetCamera.AddCommandBuffer(CameraEvent.BeforeReflections, commandBuffer);

				int normalsID = Shader.PropertyToID("_NormalsCopy");
				commandBuffer.GetTemporaryRT(normalsID, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGB2101010);

				commandBuffer.Blit(BuiltinRenderTextureType.GBuffer2, normalsID);

				if (targetCamera.allowHDR)
					mrt[3] = BuiltinRenderTextureType.CameraTarget;
				else
					mrt[3] = BuiltinRenderTextureType.GBuffer3;

				commandBuffer.SetRenderTarget(mrt, BuiltinRenderTextureType.CameraTarget);

				cutoffValues.Clear();
				colorValues.Clear();
				properties.Clear();

				// Clean material collections.
				decalMaterialOrder.RemoveAll(x => x == null);
				List<Material> destroyedMaterials = decalsByMaterial.Keys.Where(x => x == null).ToList();
				foreach(Material mat in destroyedMaterials)
					decalsByMaterial.Remove(mat);

				decalMaterialOrder.Sort(CompareMaterialOrder);

				List<int> availablePasses = new List<int>(2);
				
				foreach (Material material in decalMaterialOrder)
				{
					HashSet<DeferredDecal> decalSet = decalsByMaterial[material];

					bool anyVisible = false;
					for (int i = 0; i < decalCount; ++i)
					{
						if (decalVisibility[i])
						{
							anyVisible = true;
							break;
						}
					}
					if (!anyVisible)
						continue;

					cutoffValues.Clear();

					int instanceIndex = 0;

					availablePasses.Clear();
					for (int i = 0; i < material.passCount; ++i)
						if (material.GetShaderPassEnabled(material.GetPassName(i)))
							availablePasses.Add(i);

					foreach (var decal in decalSet)
					{
						if (!decalVisibility[decal.decalIndex])
							continue;

						if (material.enableInstancing)
						{
							matrixBatchCache[instanceIndex] = decalMatracies[decal.decalIndex];
							decal.meshRendererSurrogate.GetPropertyBlock(properties);
							float cutoffValue = properties.GetFloat(cutoffPropertyID);
							if (cutoffValue != 0)
								cutoffValues.Add(cutoffValue);
							else
								cutoffValues.Add(material.GetFloat(cutoffPropertyID));
							Color color = properties.GetColor(colorPropertyID);
							if (color.a != 0)
								colorValues.Add(color);
							else
								colorValues.Add(material.GetColor(colorPropertyID));
							if (instanceIndex == 1022)
							{
								properties.Clear();
								properties.SetFloatArray(cutoffPropertyID, cutoffValues);
								properties.SetVectorArray(colorPropertyID, colorValues);
								for(int i = 0;i<availablePasses.Count;++i)
									InstancedRenderCall(commandBuffer, material, maxInstances, properties, availablePasses[i]);
								cutoffValues.Clear();
								colorValues.Clear();
								instanceIndex = 0;
							}
							else
								++instanceIndex;
						}
						else
						{
							decal.meshRendererSurrogate.GetPropertyBlock(properties);
							for(int i = 0;i<availablePasses.Count;++i)
								RenderCall(commandBuffer, material, decalMatracies[decal.decalIndex], properties, availablePasses[i]);
						}
					}
					if (material.enableInstancing)
					{
						properties.Clear();
						properties.SetFloatArray(cutoffPropertyID, cutoffValues);
						properties.SetVectorArray(colorPropertyID, colorValues);
						for(int i = 0;i<availablePasses.Count;++i)
							InstancedRenderCall(commandBuffer, material, instanceIndex, properties, availablePasses[i]);
						cutoffValues.Clear();
						colorValues.Clear();
					}
				}
				commandBuffer.ReleaseTemporaryRT(normalsID);
			}

			private static void GenerateCubeMesh()
			{
				Vector3[] verts = new Vector3[8];
				int i = 0;
				for (float x = -0.5f; x <= 0.5f; x += 1f)
					for (float y = -0.5f; y <= 0.5f; y += 1f)
						for (float z = -0.5f; z <= 0.5f; z += 1f)
							verts[i++] = new Vector3(x, y, z);
				cubeMesh = new Mesh();
				cubeMesh.SetVertices(verts);
				cubeMesh.SetTriangles(new int[] { 0, 1, 2, 2, 1, 3, 0, 2, 6, 6, 4, 0, 2, 3, 7, 7, 6, 2, 1, 5, 7, 7, 3, 1, 4, 6, 7, 7, 5, 4, 0, 4, 5, 5, 1, 0}, 0);
			}

			private static void InstancedRenderCall(CommandBuffer buffer, Material material, int count, MaterialPropertyBlock properties)
			{
				if (material.enableInstancing)
				{
					for (int i = 0; i < material.passCount; ++i)
						if(material.GetShaderPassEnabled(material.GetPassName(i)))
							buffer.DrawMeshInstanced(cubeMesh, 0, material, i, matrixBatchCache, count, properties);
				}
			}

			private static void InstancedRenderCall(CommandBuffer buffer, Material material, int count, MaterialPropertyBlock properties, int passIndex)
			{
				if (material.enableInstancing)
					buffer.DrawMeshInstanced(cubeMesh, 0, material, passIndex, matrixBatchCache, count, properties);
			}

			private static void RenderCall(CommandBuffer buffer, Material material, Matrix4x4 matrix, MaterialPropertyBlock properties)
			{
				for (int i = 0; i < material.passCount; ++i)
					if(material.GetShaderPassEnabled(material.GetPassName(i)))
						buffer.DrawMesh(cubeMesh, matrix, material, 0, i, properties);
			}

			private static void RenderCall(CommandBuffer buffer, Material material, Matrix4x4 matrix, MaterialPropertyBlock properties, int passIndex)
			{
				buffer.DrawMesh(cubeMesh, matrix, material, 0, passIndex, properties);
			}
		}
	}
}