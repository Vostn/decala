Shader "Vostn/Decala/Projected Deferred Decal" 
{
	Properties
	{
		_FadeAngle("_FadeAngle", Range(0,2)) = 1
		_FadeDirection("_FadeDirection", Range(0,1)) = 0
		_FadeRange("_FadeRange", Range(0,2)) = 0

		_MainTex("_MainTex", 2D) = "white" {}
		_Color("_Color", Color) = (1,1,1,1)
		_Cutoff("_Cutoff", Range(0,1)) = 0.5

		_SpecularSmoothnessMap("_SpecularSmoothnessMap", 2D) = "white" {}
		_SpecularColor("_SpecularColor", Color) = (1,1,1,1)

		_MetallicScale("_MetallicScale", Range(0,1)) = 0

		_SmoothnessScale("_SmoothnessScale", Range(0,1)) = 0.5

		_NormalMap("_NormalMap", 2D) = "bump" {}
		_NormalScale("_NormalScale", Float) = 1

		_EmissionMap("_EmissionMap", 2D) = "white" {}
		_EmissionColor("_EmissionColor", Color) = (0,0,0)

		_StencilRef("_StencilRef", Float) = 16

		_SrcBlend("_SrcBlend", Float) = 5
		_DstBlend("_DstBlend", Float) = 10

		_SmoothBlendType("_SmoothBlendType", Float) = 1

		_SpecSrcBlend("_SpecSrcBlend", Float) = 5
		_SpecDstBlend("_SpecDstBlend", Float) = 10

		_TransparencyMode("_TransparencyMode", Float) = 1
	}
	SubShader
	{
		Pass
		{
			Name "First Pass"
			Fog{ Mode Off }
			ZWrite Off
			ZTest Always
			Cull Front

			Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "ForceNoShadowCasting" = "True" }

			ColorMask RGB 0
			ColorMask RGBA 1
			ColorMask RGB 2
			ColorMask RGB 3

			Blend 0 [_SrcBlend] [_DstBlend]
			Blend 1 [_SpecSrcBlend] [_SpecDstBlend], Zero OneMinusSrcAlpha
			Blend 2 [_SrcBlend] [_DstBlend]
			Blend 3 [_SrcBlend] [_DstBlend]

			//Temporary masking until proper solution can be found
			Stencil
			{
				Ref [_StencilRef]
				ReadMask 28
				WriteMask 0
				Comp NotEqual
				Pass Keep
				Fail Keep
				ZFail Keep
			}

			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag
			#pragma exclude_renderers nomrt
			#pragma multi_compile_instancing
			#pragma shader_feature _METALLIC
			#pragma shader_feature _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
			#pragma shader_feature _ALBEDOSPEC
			#pragma shader_feature _SMOOTHBLEND
			#pragma shader_feature _NORMAL1
			#pragma shader_feature _EMISSION1
			#pragma multi_compile ___ UNITY_HDR_ON

			#include "UnityCG.cginc"
			#include "UnityStandardUtils.cginc"
			#include "HLSLSupport.cginc"

			struct vin
			{
				float4 pos : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
				float4 screenUV : TEXCOORD1;
				float3 ray : TEXCOORD2;
				half3 orientation : TEXCOORD3;
				#if _NORMAL1
					half3 orientationX : TEXCOORD4;
				#endif
				float3 pivot : TEXCOORD5;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			v2f vert(vin v)
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.pos = UnityObjectToClipPos(v.pos);
				o.uv = v.pos.xz + 0.5;
				o.screenUV = ComputeScreenPos(o.pos);
				o.ray = mul(UNITY_MATRIX_MV, v.pos).xyz * float3(-1, -1, 1);
				o.orientation = normalize(mul((float3x3)unity_ObjectToWorld, float3(0, 1, 0)));
				#if _NORMAL1
					o.orientationX = normalize(mul((float3x3)unity_ObjectToWorld, float3(1, 0, 0)));
				#endif
				o.pivot = mul(unity_ObjectToWorld, float4(0, 0, 0, 1)).xyz;
				return o;
			}
			
			sampler2D _MainTex;

			sampler2D _SpecularSmoothnessMap;
			
			sampler2D _NormalMap;

			sampler2D _EmissionMap;

			sampler2D_float _CameraDepthTexture;
			sampler2D _NormalsCopy;

			UNITY_INSTANCING_BUFFER_START(Props)
				UNITY_DEFINE_INSTANCED_PROP(half, _FadeAngle)
				UNITY_DEFINE_INSTANCED_PROP(half, _FadeDirection)
				UNITY_DEFINE_INSTANCED_PROP(half, _FadeRange)

				UNITY_DEFINE_INSTANCED_PROP(half4, _MainTex_ST)
				UNITY_DEFINE_INSTANCED_PROP(half4, _Color)
				UNITY_DEFINE_INSTANCED_PROP(half, _Cutoff)

				UNITY_DEFINE_INSTANCED_PROP(half, _MetallicScale)

				UNITY_DEFINE_INSTANCED_PROP(half4, _SpecularSmoothnessMap_ST)
				UNITY_DEFINE_INSTANCED_PROP(half4, _SpecularColor)

				UNITY_DEFINE_INSTANCED_PROP(half, _SmoothnessScale)

				UNITY_DEFINE_INSTANCED_PROP(half4, _NormalMap_ST)
				UNITY_DEFINE_INSTANCED_PROP(half, _NormalScale)

				UNITY_DEFINE_INSTANCED_PROP(half4, _EmissionMap_ST)
				UNITY_DEFINE_INSTANCED_PROP(half4, _EmissionColor)
			UNITY_INSTANCING_BUFFER_END(Props)

			void frag(v2f i, 
			out half4 outAlbedo : SV_Target0, 
			out half4 outSpecSmooth : SV_Target1, 
			out half4 outNormal : SV_Target2,
			out half4 outEmission : SV_Target3)
			{
				UNITY_SETUP_INSTANCE_ID(i);

				//Project cube onto scene
				i.ray = i.ray * (_ProjectionParams.z / i.ray.z);
				float2 uv = i.screenUV.xy / i.screenUV.w;
				float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, uv);
				depth = Linear01Depth(depth);
				float4 vpos = float4(i.ray * depth, 1);
				float3 wpos = mul(unity_CameraToWorld, vpos).xyz;
				float3 opos = mul(unity_WorldToObject, float4(wpos, 1)).xyz;
				
				//Calculate cutoff angle for Fade
				half fadeDirection = UNITY_ACCESS_INSTANCED_PROP(Props, _FadeDirection);
				half3 fadeCutoffAngle = normalize(lerp(i.orientation, i.pivot - wpos, fadeDirection));

				//Clamp to prevent texture repetition outside box bounds
				clip(float3(0.5, 0.5, 0.5) - abs(opos.xyz));

				//Reproject texture sample UV
				i.uv = opos.xz + 0.5;
				
				//Sample scene normal
				half3 normal = tex2D(_NormalsCopy, uv).rgb;
				half3 wnormal = normal.rgb * 2.0 - 1.0;

				//Calculate fade angles
				half angleFromSource = dot(wnormal, fadeCutoffAngle);
				half fadeAngle = UNITY_ACCESS_INSTANCED_PROP(Props, _FadeAngle);
				half fadeRange = 0;
				#if _ALPHABLEND_ON
					fadeRange = UNITY_ACCESS_INSTANCED_PROP(Props, _FadeRange);
				#endif
				half angleFromCutoff = angleFromSource - (1.001 - (fadeAngle + (fadeRange * 0.5)) * 1.01);
				
				//Cutoff at configured fade angle
				clip(angleFromCutoff);

				//Sample Albedo value
				half4 mainTextST = UNITY_ACCESS_INSTANCED_PROP(Props, _MainTex_ST);
				half4 albedoTex = tex2D(_MainTex, i.uv * mainTextST.xy + mainTextST.zw);
				half4 color = UNITY_ACCESS_INSTANCED_PROP(Props, _Color);
				half4 albedoColor = albedoTex * color;
				
				#if _ALPHATEST_ON
					//Cutoff based on albedo alpha
					half cutoff = UNITY_ACCESS_INSTANCED_PROP(Props, _Cutoff);
					clip(albedoColor.a - cutoff);
					albedoColor.a = 1;
				#endif

				//Separate transparency for other buffers
				half transparency = albedoColor.a;

				//Modulate transparency from fade range
				transparency *= min(1, angleFromCutoff / fadeRange);

				#if _ALBEDOSPEC || _SMOOTHBLEND
					half4 specularSmoothnessMapST = UNITY_ACCESS_INSTANCED_PROP(Props, _SpecularSmoothnessMap_ST);
					half4 specSmoothTex = tex2D(_SpecularSmoothnessMap, i.uv * specularSmoothnessMapST.xy + specularSmoothnessMapST.zw);
				#endif

				//Calculate Spec and Albedo colour, and blend to MRT
				#if _ALBEDOSPEC
					half oneMinusReflectivity;
					half3 specularColor;
					#if _METALLIC
						half metallicScale = UNITY_ACCESS_INSTANCED_PROP(Props, _MetallicScale);
						albedoColor.rgb = DiffuseAndSpecularFromMetallic(albedoColor.rgb, specSmoothTex.r * metallicScale, specularColor, oneMinusReflectivity);
					#else
						specularColor = UNITY_ACCESS_INSTANCED_PROP(Props, _SpecularColor).rgb * specSmoothTex.rgb;
						albedoColor.rgb = EnergyConservationBetweenDiffuseAndSpecular(albedoColor.rgb, specularColor.rgb, oneMinusReflectivity);
					#endif
					outSpecSmooth.rgb = half4(specularColor.rgb, transparency);
					outSpecSmooth.a = transparency;
				#endif

				#if _SMOOTHBLEND && !_ALBEDOSPEC
					outSpecSmooth.a = transparency * specSmoothTex.a;
				#endif
				
				//Output albedo to MRT
				#if _ALBEDOSPEC
					#if _ALPHAPREMULTIPLY_ON
						outAlbedo = half4(albedoColor.rgb * transparency, transparency);
					#else
						outAlbedo = half4(albedoColor.rgb, transparency);
					#endif
				#endif

				//Calculate normals and blend to MRT
				float4 finalNormal = float4(wnormal, 1);
				#if _NORMAL1
					//Unpack normals from normal map
					float4 normalMapST = UNITY_ACCESS_INSTANCED_PROP(Props, _NormalMap_ST);
					float normalScale = UNITY_ACCESS_INSTANCED_PROP(Props, _NormalScale);
					float3 nor = UnpackScaleNormal(tex2D(_NormalMap, i.uv * normalMapST.xy + normalMapST.zw), normalScale);

					//Reorient the normals to the correct axes (y up on blue instead of z up on blue)
					float3x3 norMat = float3x3(float3(1, 0, 0), float3(0, 0, 1), float3(0, 1, 0));
					nor = mul(nor, norMat);

					//Re-map the normals to the scene's tangent space
					float3 cprodZ = normalize(cross(wnormal, -i.orientationX));
					float3 cprodX = normalize(cross(wnormal, cprodZ));
					norMat = float3x3(cprodX, wnormal, cprodZ);
					nor = mul(nor, norMat);

					//Recale normals and output
					finalNormal = float4(nor * 0.5 + 0.5, transparency);
					outNormal = finalNormal;
				#endif

				half3 emission = half3(0, 0, 0);
				
				//Calculate ambient light and probes, and blend into MRT
				#if _ALBEDOSPEC
					emission += ShadeSH9(float4(normalize(finalNormal.xyz), 1)).rgb * albedoColor.rgb;
				#endif

				//Calculate emission
				#if _EMISSION1
					half4 emissionMapST = UNITY_ACCESS_INSTANCED_PROP(Props, _EmissionMap_ST);
					half4 emissionSample = tex2D(_EmissionMap, i.uv * emissionMapST.xy + emissionMapST.zw);
					half4 emissionColor = UNITY_ACCESS_INSTANCED_PROP(Props, _EmissionColor);
					emission += emissionSample.rgb * emissionColor.rgb * emissionSample.a;
				#endif

				//LDR uses alternate encoding in emission
				#ifndef UNITY_HDR_ON
					emission = exp2(-emission);
				#endif
				
				//Blend emission to MRT
				#if _EMISSION1 || _ALBEDOSPEC
					outEmission = half4(emission, transparency);
				#endif
			}
			ENDCG
		}
		Pass
		{
			Name "Second Pass"
			Fog{ Mode Off }
			ZWrite Off
			ZTest Always
			Cull Front

			Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "ForceNoShadowCasting" = "True" }

			ColorMask 0 0
			ColorMask A 1
			ColorMask RGB 2
			ColorMask RGB 3
			
			Blend 0 Zero One
			Blend 1 Zero One, One [_SmoothBlendType]
			Blend 2 [_SrcBlend] [_DstBlend]
			Blend 3 [_SrcBlend] One

			//Temporary masking until proper solution can be found
			Stencil
			{
				Ref [_StencilRef]
				ReadMask 28
				WriteMask 0
				Comp NotEqual
				Pass Keep
				Fail Keep
				ZFail Keep
			}

			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag
			#pragma exclude_renderers nomrt
			#pragma multi_compile_instancing
			#pragma shader_feature _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
			#pragma shader_feature _SMOOTH
			#pragma shader_feature _SMOOTHBLEND
			#pragma shader_feature _NORMAL2
			#pragma shader_feature _EMISSION2
			#pragma multi_compile ___ UNITY_HDR_ON

			#include "UnityCG.cginc"
			#include "UnityStandardUtils.cginc"
			#include "HLSLSupport.cginc"

			struct vin
			{
				float4 pos : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
				float4 screenUV : TEXCOORD1;
				float3 ray : TEXCOORD2;
				half3 orientation : TEXCOORD3;
				#if _NORMAL2
					half3 orientationX : TEXCOORD4;
				#endif
				float3 pivot : TEXCOORD5;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			v2f vert(vin v)
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.pos = UnityObjectToClipPos(v.pos);
				o.uv = v.pos.xz + 0.5;
				o.screenUV = ComputeScreenPos(o.pos);
				o.ray = mul(UNITY_MATRIX_MV, v.pos).xyz * float3(-1, -1, 1);
				o.orientation = normalize(mul((float3x3)unity_ObjectToWorld, float3(0, 1, 0)));
				#if _NORMAL2
					o.orientationX = normalize(mul((float3x3)unity_ObjectToWorld, float3(1, 0, 0)));
				#endif
				o.pivot = mul(unity_ObjectToWorld, float4(0, 0, 0, 1)).xyz;
				return o;
			}
			
			sampler2D _MainTex;

			sampler2D _SpecularSmoothnessMap;

			sampler2D _NormalMap;

			sampler2D _EmissionMap;

			sampler2D_float _CameraDepthTexture;
			sampler2D _NormalsCopy;

			UNITY_INSTANCING_BUFFER_START(Props)
				UNITY_DEFINE_INSTANCED_PROP(half, _FadeAngle)
				UNITY_DEFINE_INSTANCED_PROP(half, _FadeDirection)
				UNITY_DEFINE_INSTANCED_PROP(half, _FadeRange)

				UNITY_DEFINE_INSTANCED_PROP(half4, _MainTex_ST)
				UNITY_DEFINE_INSTANCED_PROP(half4, _Color)
				UNITY_DEFINE_INSTANCED_PROP(half, _Cutoff)

				UNITY_DEFINE_INSTANCED_PROP(half4, _NormalMap_ST)
				UNITY_DEFINE_INSTANCED_PROP(half, _NormalScale)

				UNITY_DEFINE_INSTANCED_PROP(half4, _SpecularSmoothnessMap_ST)

				UNITY_DEFINE_INSTANCED_PROP(half, _SmoothnessScale)

				UNITY_DEFINE_INSTANCED_PROP(half4, _EmissionMap_ST)
				UNITY_DEFINE_INSTANCED_PROP(half4, _EmissionColor)
			UNITY_INSTANCING_BUFFER_END(Props)

			void frag(v2f i,
				out half4 outAlbedo : SV_Target0,
				out half4 outSpecSmooth : SV_Target1,
				out half4 outNormal : SV_Target2,
				out half4 outEmission : SV_Target3)
			{
				UNITY_SETUP_INSTANCE_ID(i);

				//Project cube onto scene
				i.ray = i.ray * (_ProjectionParams.z / i.ray.z);
				float2 uv = i.screenUV.xy / i.screenUV.w;
				float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, uv);
				depth = Linear01Depth(depth);
				float4 vpos = float4(i.ray * depth, 1);
				float3 wpos = mul(unity_CameraToWorld, vpos).xyz;
				float3 opos = mul(unity_WorldToObject, float4(wpos, 1)).xyz;

				//Calculate cutoff angle for Fade
				half fadeDirection = UNITY_ACCESS_INSTANCED_PROP(Props, _FadeDirection);
				half3 fadeCutoffAngle = normalize(lerp(i.orientation, i.pivot - wpos, fadeDirection));

				//Clamp to prevent texture repetition outside box bounds
				clip(float3(0.5, 0.5, 0.5) - abs(opos.xyz));

				//Reproject texture sample UV
				i.uv = opos.xz + 0.5;

				//Sample scene normal
				half3 normal = tex2D(_NormalsCopy, uv).rgb;
				half3 wnormal = normal.rgb * 2.0 - 1.0;

				//Calculate fade angles
				half angleFromSource = dot(wnormal, fadeCutoffAngle);
				half fadeAngle = UNITY_ACCESS_INSTANCED_PROP(Props, _FadeAngle);
				half fadeRange = 0;
				#if _ALPHABLEND_ON
					fadeRange = UNITY_ACCESS_INSTANCED_PROP(Props, _FadeRange);
				#endif
				half angleFromCutoff = angleFromSource - (1.001 - (fadeAngle + (fadeRange * 0.5)) * 1.01);

				//Cutoff at configured fade angle
				clip(angleFromCutoff);

				//Sample Albedo value
				half4 mainTextST = UNITY_ACCESS_INSTANCED_PROP(Props, _MainTex_ST);
				half4 albedoTex = tex2D(_MainTex, i.uv * mainTextST.xy + mainTextST.zw);
				half4 color = UNITY_ACCESS_INSTANCED_PROP(Props, _Color);
				half4 albedoColor = albedoTex * color;

				#if _ALPHATEST_ON
					//Cutoff based on albedo alpha
					half cutoff = UNITY_ACCESS_INSTANCED_PROP(Props, _Cutoff);
					clip(albedoColor.a - cutoff);
					albedoColor.a = 1;
				#endif

				//Separate transparency for other buffers
				half transparency = albedoColor.a;

				//Modulate transparency from fade range
				transparency *= min(1, angleFromCutoff / fadeRange);
				
				//Calculate normals and blend to MRT
				#if _NORMAL2
					float4 finalNormal = float4(wnormal, 1);
					//Unpack normals from normal map
					float4 normalMapST = UNITY_ACCESS_INSTANCED_PROP(Props, _NormalMap_ST);
					float normalScale = UNITY_ACCESS_INSTANCED_PROP(Props, _NormalScale);
					float3 nor = UnpackScaleNormal(tex2D(_NormalMap, i.uv * normalMapST.xy + normalMapST.zw), normalScale);

					//Reorient the normals to the correct axes (y up on blue instead of z up on blue)
					float3x3 norMat = float3x3(float3(1, 0, 0), float3(0, 0, 1), float3(0, 1, 0));
					nor = mul(nor, norMat);

					//Re-map the normals to the scene's tangent space
					float3 cprodZ = normalize(cross(wnormal, -i.orientationX));
					float3 cprodX = normalize(cross(wnormal, cprodZ));
					norMat = float3x3(cprodX, wnormal, cprodZ);
					nor = mul(nor, norMat);

					//Recale normals and output
					finalNormal = float4(nor * 0.5 + 0.5, transparency);
					outNormal = finalNormal;
				#endif

				//Calculate smooth and write into MRT
				//Sample texture
				#if _SMOOTH
					half4 specSmoothnessMapST = UNITY_ACCESS_INSTANCED_PROP(Props, _SpecularSmoothnessMap_ST);
					half4 specSmoothTex = tex2D(_SpecularSmoothnessMap, i.uv * specSmoothnessMapST.xy + specSmoothnessMapST.zw);
					//Multiply by scale, and texture alpha
					half smoothnessScale = UNITY_ACCESS_INSTANCED_PROP(Props, _SmoothnessScale);
					outSpecSmooth.a = smoothnessScale * specSmoothTex.a;
					outSpecSmooth.a = max(0.0001, sqrt(outSpecSmooth.a));

					#if !_SMOOTHBLEND
						//Use round to calculate custom clip value
						half smoothnessCutoff = UNITY_ACCESS_INSTANCED_PROP(Props, _Cutoff);
						half rounded = ceil(transparency - smoothnessCutoff);
						outSpecSmooth.a *= rounded;
						//Clip result
						clip(outSpecSmooth.a - 0.0001);
					#else
						// Multiply by albedo alpha.
						outSpecSmooth.a *= transparency;
					#endif
				#endif

				//Calculate emission
				#if _EMISSION2
					half3 emission = half3(0, 0, 0);
					half4 emissionMapST = UNITY_ACCESS_INSTANCED_PROP(Props, _EmissionMap_ST);
					half4 emissionSample = tex2D(_EmissionMap, i.uv * emissionMapST.xy + emissionMapST.zw);
					half4 emissionColor = UNITY_ACCESS_INSTANCED_PROP(Props, _EmissionColor);
					emission += emissionSample.rgb * emissionColor.rgb * emissionSample.a;

					//LDR uses alternate encoding in emission
					#ifndef UNITY_HDR_ON
						emission = exp2(-emission);
					#endif
					
					//Blend emission to MRT
					outEmission = half4(emission, transparency);
				#endif
			}
			ENDCG
		}
	}
	Fallback Off
	CustomEditor "Vostn.Decala.Editor.DeferredDecal_ShaderGUI"
}